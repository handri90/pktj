
DROP TABLE IF EXISTS `level_user`;

CREATE TABLE `level_user` (
  `id_level_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level_user` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_level_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `level_user` */

insert  into `level_user`(`id_level_user`,`nama_level_user`,`created_at`,`deleted_at`,`updated_at`) values 
(1,'superadmin','2021-01-05 08:21:55',NULL,NULL);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(50) DEFAULT NULL,
  `id_parent_menu` int(11) DEFAULT NULL,
  `nama_module` varchar(100) DEFAULT NULL,
  `nama_class` varchar(100) DEFAULT NULL,
  `class_icon` varchar(20) DEFAULT NULL,
  `order_menu` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

insert  into `menu`(`id_menu`,`nama_menu`,`id_parent_menu`,`nama_module`,`nama_class`,`class_icon`,`order_menu`,`created_at`,`deleted_at`,`updated_at`) values 
(1,'User',0,'user','User','icon-user',2,'2020-01-14 10:16:58',NULL,'2020-01-26 11:44:32'),
(5,'Dashboard',0,'dashboard','Dashboard','icon-home4',1,'2020-01-14 14:06:31',NULL,NULL),
(6,'Level User',0,'level_user','Level_user','icon-accessibility',3,'2020-01-14 14:21:49',NULL,'2020-01-26 11:45:08'),
(7,'Menu',0,'menu','Menu','icon-menu4',4,'2020-01-19 13:40:22',NULL,'2020-01-26 11:47:37'),
(9,'Privilage Menu',0,'privilage_level','Privilage_level','icon-list',5,'2020-01-19 11:22:27',NULL,'2020-01-26 11:47:04');

/*Table structure for table `privilage_level_menu` */

DROP TABLE IF EXISTS `privilage_level_menu`;

CREATE TABLE `privilage_level_menu` (
  `id_privilage` int(11) NOT NULL AUTO_INCREMENT,
  `level_user_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `create_content` enum('1','0') DEFAULT NULL,
  `update_content` enum('1','0') DEFAULT NULL,
  `delete_content` enum('1','0') DEFAULT NULL,
  `view_content` enum('1','0') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_privilage`),
  KEY `fk_privilage_level_menu` (`level_user_id`),
  KEY `fk_privilage_menu_id` (`menu_id`),
  CONSTRAINT `fk_privilage_level_menu` FOREIGN KEY (`level_user_id`) REFERENCES `level_user` (`id_level_user`),
  CONSTRAINT `fk_privilage_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `privilage_level_menu` */

insert  into `privilage_level_menu`(`id_privilage`,`level_user_id`,`menu_id`,`create_content`,`update_content`,`delete_content`,`view_content`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,1,'1','1','1','1','2020-01-17 14:20:33','2021-01-05 08:44:52',NULL),
(2,1,5,'1','1','1','1','2020-01-17 14:20:54','2020-09-20 03:05:31','2021-01-05 08:44:39'),
(4,1,6,'1','1','1','1','2020-01-17 14:33:15','2020-09-20 03:05:31','2020-09-04 02:02:23'),
(5,1,7,'1','1','1','1','2020-01-19 13:41:58','2021-01-05 08:44:52',NULL),
(6,1,9,'1','1','1','1','2020-01-19 23:23:38','2021-01-05 08:44:52',NULL),
(7,1,5,'1','1','1','1','2021-01-05 08:44:52',NULL,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level_user_id` int(11) DEFAULT NULL,
  `foto_user` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `fk_level_user` (`level_user_id`),
  CONSTRAINT `fk_level_user` FOREIGN KEY (`level_user_id`) REFERENCES `level_user` (`id_level_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id_user`,`nama_lengkap`,`username`,`password`,`level_user_id`,`foto_user`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Superadmin','superadmin','$2y$12$2d2GEFgIkv/WhniVgh4aFuQFRLryQ1BOn6CnZBmft2XP2WR.MP7ua',1,'85f8e428e5f5e4f541eb4ccb39b0ddaf.png','2021-01-05 08:22:34','2021-01-05 08:44:14',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
