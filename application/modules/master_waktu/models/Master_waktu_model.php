<?php

class Master_waktu_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_waktu";
        $this->primary_id = "id_master_waktu";
    }
}
