<div class="content">
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url(); ?>master_waktu/tambah_master_waktu" class="btn btn-info">Tambah Setting Waktu</a>
            </div>
        </div>
        <table id="datatableWaktu" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Waktu</th>
                    <th>Jumlah Hari</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<script>
    let datatableWaktu = $("#datatableWaktu").DataTable({
        "columns": [
            null,
            null,
            {
                "width": "20%"
            }
        ]
    });
    get_data_master_waktu();

    function get_data_master_waktu() {
        datatableWaktu.clear().draw();
        $.ajax({
            url: base_url + 'master_waktu/request/get_data_master_waktu',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableWaktu.row.add([
                        value.nama_waktu,
                        value.jumlah_hari,
                        "<a href='" + base_url + "master_waktu/edit_master_waktu/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_master_waktu) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'master_waktu/delete_master_waktu',
                    data: {
                        id_master_waktu: id_master_waktu
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_master_waktu();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_master_waktu();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_master_waktu();
                    }
                });
            }
        });
    }
</script>