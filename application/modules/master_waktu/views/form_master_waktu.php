<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open(); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Waktu</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Waktu <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_waktu : ""; ?>" name="nama_waktu" placeholder="Waktu">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Jumlah Hari <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->jumlah_hari : ""; ?>" name="jumlah_hari" required placeholder="Jumlah Hari">
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->
</div>