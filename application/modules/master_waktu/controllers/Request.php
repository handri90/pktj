<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_waktu_model');
    }

    public function get_data_master_waktu()
    {
        $data_master_waktu = $this->master_waktu_model->get(
            array(
                'order_by' => array(
                    'nama_waktu' => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_master_waktu as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_waktu);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
