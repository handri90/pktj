<?php
defined('BASEPATH') or exit('No direct script access allowed');

class master_waktu extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_waktu_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Master Waktu', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_master_waktu()
    {
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'master_waktu', 'content' => 'Master Waktu', 'is_active' => false], ['link' => false, 'content' => 'Tambah Master Waktu', 'is_active' => true]];
            $this->execute('form_master_waktu', $data);
        } else {

            $data = array(
                "nama_waktu" => $this->ipost('nama_waktu'),
                "jumlah_hari" => $this->ipost('jumlah_hari'),
                'created_at' => $this->datetime()
            );

            $status = $this->master_waktu_model->save($data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('master_waktu');
        }
    }

    public function edit_master_waktu($id_master_waktu)
    {
        $data_master = $this->master_waktu_model->get_by(decrypt_data($id_master_waktu));

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'master_waktu', 'content' => 'Master Waktu', 'is_active' => false], ['link' => false, 'content' => 'Tambah Master Waktu', 'is_active' => true]];
            $this->execute('form_master_waktu', $data);
        } else {
            $data = array(
                "nama_waktu" => $this->ipost('nama_waktu'),
                "jumlah_hari" => $this->ipost('jumlah_hari'),
                'updated_at' => $this->datetime()
            );

            $status = $this->master_waktu_model->edit(decrypt_data($id_master_waktu), $data);
            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('master_waktu');
        }
    }

    public function delete_master_waktu()
    {
        $id_master_waktu = decrypt_data($this->iget('id_master_waktu'));
        $data_master = $this->master_waktu_model->get_by($id_master_waktu);

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->master_waktu_model->remove($id_master_waktu);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
