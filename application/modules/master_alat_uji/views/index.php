<div class="content">
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url(); ?>master_alat_uji/tambah_master_alat_uji" class="btn btn-info">Tambah Alat Uji</a>
            </div>
        </div>
        <table id="datatableAlatUji" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Nama Alat Uji</th>
                    <th>Yang Perlu Diperiksa / SOP Pemeriksaan</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<script>
    let datatableAlatUji = $("#datatableAlatUji").DataTable({
        "columns": [{
                "width": "20%"
            },
            null,
            {
                "width": "20%"
            }
        ]
    });
    get_data_master_alat_uji();

    function get_data_master_alat_uji() {
        datatableAlatUji.clear().draw();
        $.ajax({
            url: base_url + 'master_alat_uji/request/get_data_master_alat_uji',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let spl_nama_indikator = value.nama_indikator.split("|");
                    let spl_sop_pemeriksaan = value.sop_pemeriksaan.split("|");
                    let html = "";
                    html += "<table>";
                    $.each(spl_nama_indikator, function(index_spl, val_spl) {
                        html += "<tr>" +
                            "<td>" + val_spl + "</td>" +
                            "<td>" + spl_sop_pemeriksaan[index_spl] + "</td>" +
                            "</tr>";
                    });
                    html += "</tr>";

                    datatableAlatUji.row.add([
                        value.nama_alat_uji,
                        html,
                        "<a href='" + base_url + "master_alat_uji/edit_master_alat_uji/" + value.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_master_alat_uji) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'master_alat_uji/delete_master_alat_uji',
                    data: {
                        id_master_alat_uji: id_master_alat_uji
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_master_alat_uji();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_master_alat_uji();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_master_alat_uji();
                    }
                });
            }
        });
    }
</script>