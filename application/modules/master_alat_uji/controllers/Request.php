<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_alat_uji_model');
    }

    public function get_data_master_alat_uji()
    {
        $data_master_alat_uji = $this->master_alat_uji_model->get(
            array(
                "fields" => "master_alat_uji.*,GROUP_CONCAT(nama_indikator ORDER BY id_detail_indikator_alat_uji SEPARATOR '|') as nama_indikator,GROUP_CONCAT(sop_pemeriksaan ORDER BY id_detail_indikator_alat_uji SEPARATOR '|') as sop_pemeriksaan",
                "join" => array(
                    "detail_indikator_alat_uji" => "master_alat_uji_id=id_master_alat_uji"
                ),
                'order_by' => array(
                    'nama_alat_uji' => "ASC"
                ),
                "group_by" => "id_master_alat_uji"
            )
        );

        $templist = array();
        foreach ($data_master_alat_uji as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_alat_uji);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
