<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_alat_uji extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_alat_uji_model');
        $this->load->model('detail_indikator_alat_uji_model');
    }

    public function index()
    {
        $data['breadcrumb'] = [['link' => false, 'content' => 'Master Alat Uji', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_master_alat_uji()
    {
        if (empty($_POST)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'master_alat_uji', 'content' => 'Master Alat Uji', 'is_active' => false], ['link' => false, 'content' => 'Tambah Master Alat Uji', 'is_active' => true]];
            $this->execute('form_master_alat_uji', $data);
        } else {
            $data = array(
                "nama_alat_uji" => $this->ipost('nama_alat_uji'),
                'created_at' => $this->datetime()
            );

            $status = $this->master_alat_uji_model->save($data);

            foreach ($this->ipost("indikator") as $key => $val) {
                $data_detail_indikator = array(
                    "nama_indikator" => $val,
                    "sop_pemeriksaan" => $this->ipost("sop_pemeriksaan")[$key],
                    "master_alat_uji_id" => $status,
                    'created_at' => $this->datetime()
                );

                $this->detail_indikator_alat_uji_model->save($data_detail_indikator);
            }

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }

            redirect('master_alat_uji');
        }
    }

    public function edit_master_alat_uji($id_master_alat_uji)
    {
        $data_master = $this->master_alat_uji_model->get(
            array(
                "fields" => "master_alat_uji.*,
                GROUP_CONCAT(IFNULL(id_detail_indikator_alat_uji,'') ORDER BY id_detail_indikator_alat_uji SEPARATOR '|') AS id_detail_indikator_alat_uji,
                GROUP_CONCAT(IFNULL(nama_indikator,'') ORDER BY id_detail_indikator_alat_uji SEPARATOR '|') AS nama_indikator,
                GROUP_CONCAT(IFNULL(sop_pemeriksaan,'') ORDER BY id_detail_indikator_alat_uji SEPARATOR '|') AS sop_pemeriksaan",
                "where" => array(
                    "id_master_alat_uji" => decrypt_data($id_master_alat_uji)
                ),
                "join" => array(
                    "detail_indikator_alat_uji" => "id_master_alat_uji=master_alat_uji_id AND detail_indikator_alat_uji.deleted_at IS NULL"
                ),
                "group_by" => "id_master_alat_uji"
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        if (empty($_POST)) {
            $data['content'] = $data_master;
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'master_alat_uji', 'content' => 'Master Alat Uji', 'is_active' => false], ['link' => false, 'content' => 'Tambah Master Alat Uji', 'is_active' => true]];
            $this->execute('form_master_alat_uji', $data);
        } else {
            $data = array(
                "nama_alat_uji" => $this->ipost('nama_alat_uji'),
                'updated_at' => $this->datetime()
            );

            $status = $this->master_alat_uji_model->edit(decrypt_data($id_master_alat_uji), $data);

            if ($this->ipost("delete_indiaktor")) {
                foreach ($this->ipost("delete_indiaktor") as $key => $val) {
                    if ($val) {
                        $this->detail_indikator_alat_uji_model->remove($val);
                    }
                }
            }

            foreach ($this->ipost("id_detail_indikator") as $key => $val) {
                $data_detail_indikator = array(
                    "nama_indikator" => $this->ipost("old_indikator")[$val],
                    "sop_pemeriksaan" => $this->ipost("old_sop_pemeriksaan")[$val],
                    'updated_at' => $this->datetime()
                );

                $this->detail_indikator_alat_uji_model->edit($val, $data_detail_indikator);
            }

            foreach ($this->ipost("indikator") as $key => $val) {
                $data_detail_indikator = array(
                    "nama_indikator" => $val,
                    "sop_pemeriksaan" => $this->ipost("sop_pemeriksaan")[$key],
                    "master_alat_uji_id" => decrypt_data($id_master_alat_uji),
                    'created_at' => $this->datetime()
                );

                $this->detail_indikator_alat_uji_model->save($data_detail_indikator);
            }

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('master_alat_uji');
        }
    }

    public function delete_master_alat_uji()
    {
        $id_master_alat_uji = decrypt_data($this->iget('id_master_alat_uji'));
        $data_master = $this->master_alat_uji_model->get_by($id_master_alat_uji);

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->master_alat_uji_model->remove($id_master_alat_uji);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
