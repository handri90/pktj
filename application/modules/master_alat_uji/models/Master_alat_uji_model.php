<?php

class Master_alat_uji_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_alat_uji";
        $this->primary_id = "id_master_alat_uji";
    }
}
