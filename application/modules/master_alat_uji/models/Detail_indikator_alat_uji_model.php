<?php

class Detail_indikator_alat_uji_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "detail_indikator_alat_uji";
        $this->primary_id = "id_detail_indikator_alat_uji";
    }
}
