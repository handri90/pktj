<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['breadcrumb'] = [];
        if ($this->session->userdata("level_user_id") == '3') {
            $this->execute('index_petugas', $data);
        } else {
            $this->execute('index', $data);
        }
    }
}
