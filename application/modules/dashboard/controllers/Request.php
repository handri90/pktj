<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_alat_uji/master_alat_uji_model', 'master_alat_uji_model');
    }

    public function get_schedule_pemeriksaan()
    {
        $data_alat_uji = $this->master_alat_uji_model->get(
            array(
                "fields" => "master_alat_uji.*",
                "join" => array(
                    "detail_indikator_alat_uji" => "master_alat_uji_id=id_master_alat_uji",
                    "setting_waktu_alat_uji" => "detail_indikator_alat_uji_id=id_detail_indikator_alat_uji"
                ),
                "where" => array(
                    "petugas_pemroses" => $this->session->userdata("id_user")
                ),
                "order_by" => array(
                    "nama_alat_uji" => "ASC"
                ),
                "group_by" => "id_master_alat_uji"
            )
        );

        $date_now = strtotime(date("Y-m-d"));

        $templist = array();
        foreach ($data_alat_uji as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_alat_uji);

            $data_detail_indikator = $this->master_alat_uji_model->query(
                "
                SELECT 
                    master_waktu.id_master_waktu,
                    nama_waktu,
                    jumlah_hari,
                    a.nama_indikator,
                    IFNULL(b.tanggal_pemeriksaan_last_realtime,tanggal_pemeriksaan_terakhir) AS tanggal_pemeriksaan
                FROM master_waktu
                INNER JOIN setting_waktu_alat_uji ON master_waktu.id_master_waktu = master_waktu_id
                INNER JOIN detail_indikator_alat_uji ON id_detail_indikator_alat_uji=detail_indikator_alat_uji_id
                INNER JOIN 
                (
                    SELECT GROUP_CONCAT(nama_indikator SEPARATOR '|') AS nama_indikator,id_master_waktu
                    FROM detail_indikator_alat_uji
                    INNER JOIN setting_waktu_alat_uji ON id_detail_indikator_alat_uji=detail_indikator_alat_uji_id
                    INNER JOIN master_waktu ON id_master_waktu = master_waktu_id
                    WHERE petugas_pemroses = '{$this->session->userdata("id_user")}' AND master_alat_uji_id = '{$row->id_master_alat_uji}'
                    GROUP BY id_master_waktu
                    ORDER BY nama_indikator
                ) AS a ON a.id_master_waktu=master_waktu.id_master_waktu
                LEFT JOIN 
                (
                    SELECT tanggal_pemeriksaan AS tanggal_pemeriksaan_last_realtime,setting_waktu_alat_uji_id
                    FROM trx_pemeriksaan_alat_uji
                    WHERE id_trx_pemeriksaan_alat_uji IN 
                    (
                        SELECT MAX(id_trx_pemeriksaan_alat_uji) AS id_trx_pemeriksaan_alat_uji
                        FROM trx_pemeriksaan_alat_uji
                        GROUP BY setting_waktu_alat_uji_id
                    )
                ) AS b ON b.setting_waktu_alat_uji_id=id_setting_waktu_alat_uji
                WHERE petugas_pemroses = '{$this->session->userdata("id_user")}' AND master_alat_uji_id = '{$row->id_master_alat_uji}'
                GROUP BY master_waktu.id_master_waktu
                "
            )->result();

            $html_tr = "<tbody>";
            foreach ($data_detail_indikator as $key_indikator => $row_indikator) {
                $tggl_last_terakhir = $tggl_last_terakhir = date_create($row_indikator->tanggal_pemeriksaan);;
                $data_last_terakhir = date_format(date_add($tggl_last_terakhir, date_interval_create_from_date_string($row_indikator->jumlah_hari . " days")), 'Y-m-d');
                $tggl_last_pemeriksaan = date_format(date_create($data_last_terakhir), 'Y-m-d');
                $status = "";

                if ($date_now < strtotime($tggl_last_pemeriksaan)) {
                    $status = "<span class='badge badge-success'>Terjadwal</span>";
                } else if ($date_now == strtotime($tggl_last_pemeriksaan)) {
                    $status = "<span class='badge badge-info'>Pemeriksaan Hari Ini</span>";
                } else if ($date_now > strtotime($tggl_last_pemeriksaan)) {
                    $status = "<span class='badge badge-warning'>Jadwal Pemeriksaan Sudah Lewat</span>";
                }

                $html_tr .= "<tr class='table-active table-border-double bg-blue-300'>
                <td>{$row_indikator->nama_waktu}</td>
                <td>" . longdate_indo($row_indikator->tanggal_pemeriksaan) . "</td>
                <td>" . longdate_indo($data_last_terakhir) . "</td>
                <td>" . $status . "</td>
                </tr>";

                $expl_indikator = explode("|", $row_indikator->nama_indikator);
                foreach ($expl_indikator as $key_expl_indikator => $row_expl_indikator) {
                    $html_tr .= "<tr><td colspan='5'>{$row_expl_indikator}</td></tr>";
                }
            }
            $html_tr .= "</tbody>";

            $templist[$key]['detail_waktu_indikator'] = $html_tr;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
