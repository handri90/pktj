<!-- Content area -->
<div class="content list-schedule-pemeriksaan">

</div>
<!-- /content area -->

<script>
    get_schedule_pemeriksaan();

    $(".list-schedule-pemeriksaan").html("");

    function get_schedule_pemeriksaan() {
        $.ajax({
            url: base_url + 'dashboard/request/get_schedule_pemeriksaan',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let html = "";
                $.each(response, function(index, value) {
                    html += "<div class='card border-top-success'>" +
                        "<div class='card-header header-elements-sm-inline'>" +
                        "<h6 class='card-title'>" + value.nama_alat_uji + "</h6>" +
                        "</div>" +
                        "<div class='card-body'>" +
                        "<table class='table datatable-save-state'>" +
                        "<thead>" +
                        "<tr class='bg-blue-800'>" +
                        "<th width='25%'>Waktu</th>" +
                        "<th width='25%'>Tanggal Terakhir Pemeriksaan</th>" +
                        "<th width='25%'>Jadwal Pemeriksaan</th>" +
                        "<th width='25%'>Status</th>" +
                        "</tr>" +
                        "</thead>";
                    html += value.detail_waktu_indikator;
                    html += "</table>" +
                        "</div>" +
                        "</div>";
                });
                $(".list-schedule-pemeriksaan").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>