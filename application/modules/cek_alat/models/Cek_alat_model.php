<?php

class Cek_alat_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_pemeriksaan_alat_uji";
        $this->primary_id = "id_trx_pemeriksaan_alat_uji";
    }
}
