<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Alat Uji : </label>
                        <select class="form-control select-search" name="alat_uji" onChange="get_waktu()">
                            <option value="">-- Pilih Alat Uji --</option>
                            <?php
                            foreach ($alat_uji as $key => $row) {
                            ?>
                                <option value="<?php echo encrypt_data($row->id_master_alat_uji); ?>"><?php echo $row->nama_alat_uji; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Waktu Pemeliharaan : </label>
                        <select class="form-control select-search" name="waktu" onChange="get_log()">
                            <option value="">-- Pilih --</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic datatable -->
    <div class="card">
        <div class="card-body add-hidden">
            <div class="text-right">
                <a href="#" onclick="show_form()" class="btn btn-info">Tambah Waktu Pemeriksaan</a>
            </div>
        </div>
        <table id="datatableAlatUji" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Tanggal Pemeriksaan</th>
                    <th>Hasil Pemeriksaan</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<div id="modalCekAlatUji" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Cek Alat Uji</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <form id="formSubmit" method="POST">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Alat Uji</label>
                        <div class="col-lg-9">
                            <label class="col-form-label nama-alat-uji"></label>
                            <input type="hidden" name="id_master_alat_uji" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Waktu Pemeliharaan</label>
                        <div class="col-lg-9">
                            <label class="col-form-label nama-waktu"></label>
                            <input type="hidden" name="id_master_waktu" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Tanggal Pemeriksaan</label>
                        <div class="col-lg-9">
                            <input type="text" name="tanggal_pemeriksaan" class="form-control pickadate-disable" placeholder="Tanggal Pemeriksaan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <table class='table table-bordered'>
                            <thead>
                                <tr>
                                    <th width="60%">Yang Diperiksa</th>
                                    <th width="20%" class='text-center'>Baik</th>
                                    <th width="20%" class='text-center'>Tidak Baik</th>
                                </tr>
                            </thead>
                            <tbody class="list-indikator">
                            </tbody>
                        </table>
                    </div>

                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(".add-hidden").hide();
    let datatableAlatUji = $("#datatableAlatUji").DataTable({
        "ordering": false,
        "columns": [
            null,
            null,
            {
                "width": "20%"
            }
        ]
    });

    let $input;
    let picker;

    function get_waktu() {
        let alat_uji = $("select[name='alat_uji']").val();
        let html = "<option value=''>-- Pilih --</option>";

        datatableAlatUji.clear().draw();

        if (alat_uji) {
            $.ajax({
                url: base_url + 'cek_alat/request/get_waktu',
                data: {
                    alat_uji: alat_uji
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_encrypt + "'>" + value.nama_waktu + "</option>";
                    });
                    $("select[name='waktu']").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='waktu']").html(html);
            $(".add-hidden").hide();
        }
    }

    function get_log() {
        let alat_uji = $("select[name='alat_uji']").val();
        let waktu = $("select[name='waktu']").val();

        datatableAlatUji.clear().draw();

        if (waktu) {
            $(".add-hidden").show();

            $.ajax({
                url: base_url + 'cek_alat/request/get_log',
                data: {
                    alat_uji: alat_uji,
                    waktu: waktu
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        let spl_nama_indikator = value.nama_indikator.split("|");
                        let spl_hasil_pemeriksaan = value.hasil_pemeriksaan.split("|");
                        let html = "";
                        html += "<table class='table table-bordered'>";
                        $.each(spl_nama_indikator, function(index_spl, val_spl) {
                            html += "<tr>" +
                                "<td>" + val_spl + "</td>" +
                                "<td>" + (spl_hasil_pemeriksaan[index_spl] == '1' ? 'Baik' : 'Tidak Baik') + "</td>" +
                                "</tr>";
                        });
                        html += "</tr>";

                        datatableAlatUji.row.add([
                            value.tanggal_pemeriksaan_custom,
                            html,
                            "<a href='#' onClick=\"show_form_edit('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                        ]).draw(false);
                    });
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".add-hidden").hide();
        }
    }

    function confirm_delete(id_trx_cek_alat) {
        let alat_uji = $("select[name='alat_uji']").val();
        let waktu = $("select[name='waktu']").val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'cek_alat/delete_trx_alat_uji',
                    data: {
                        alat_uji: alat_uji,
                        waktu: waktu,
                        id_trx_cek_alat: id_trx_cek_alat
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_log();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_log();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_log();
                    }
                });
            }
        });
    }

    function show_form() {
        $input = $('.pickadate-disable').pickadate({
            format: 'd-m-yyyy',
        });
        picker = $input.pickadate('picker');
        picker.start();

        let alat_uji = $("select[name='alat_uji']").val();
        let waktu = $("select[name='waktu']").val();

        $("input[name='id_master_alat_uji']").val("");
        $("input[name='id_master_waktu']").val("");

        $.ajax({
            url: base_url + 'cek_alat/request/get_set_waktu',
            data: {
                alat_uji: alat_uji,
                waktu: waktu
            },
            async: false,
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $(".alert_form").html("");
                $("#modalCekAlatUji").modal("show");
                $(".title_modal").html("Tambah");
                let alat_uji_html = $("select[name='alat_uji'] option:selected").html();
                $(".nama-alat-uji").html(alat_uji_html);
                $("input[name='id_master_alat_uji']").val(alat_uji);
                let waktu_html = $("select[name='waktu'] option:selected").html();
                $(".nama-waktu").html(waktu_html);
                $("input[name='id_master_waktu']").val(waktu);
                get_indikator();

                picker.set('min', response.min);
                picker.set('max', response.max);

                let h = [];
                h.push({
                    from: response.min,
                    to: response.max
                });

                $.each(response.arr_inverted, function(index, value) {
                    h.push(value);
                });

                $('.pickadate-disable').val('');
                picker.set('enable', true);
                picker.set('disable', h);
                console.log(picker.get('disable'));
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function show_form_edit(id_trx_cek_alat) {
        $input = $('.pickadate-disable').pickadate({
            format: 'd-m-yyyy',
        });
        picker = $input.pickadate('picker');
        picker.start();

        let alat_uji = $("select[name='alat_uji']").val();
        let waktu = $("select[name='waktu']").val();

        $("input[name='id_master_alat_uji']").val("");
        $("input[name='id_master_waktu']").val("");

        $.ajax({
            url: base_url + 'cek_alat/request/get_set_waktu',
            data: {
                alat_uji: alat_uji,
                waktu: waktu,
                id_trx_cek_alat: id_trx_cek_alat
            },
            async: false,
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $(".alert_form").html("");
                $("#modalCekAlatUji").modal("show");
                $(".title_modal").html("Tambah");
                let alat_uji_html = $("select[name='alat_uji'] option:selected").html();
                $(".nama-alat-uji").html(alat_uji_html);
                $("input[name='id_master_alat_uji']").val(alat_uji);
                let waktu_html = $("select[name='waktu'] option:selected").html();
                $(".nama-waktu").html(waktu_html);
                $("input[name='id_master_waktu']").val(waktu);
                get_indikator(id_trx_cek_alat);

                picker.set('min', response.min);
                picker.set('max', response.max);

                let h = [];
                h.push({
                    from: response.min,
                    to: response.max
                });

                $.each(response.arr_inverted, function(index, value) {
                    h.push(value);
                });

                $('.pickadate-disable').val('');

                picker.set('enable', true);
                picker.set('disable', h);
                picker.set('select', response.tanggal_edit_pemeriksaan, {
                    format: 'd-m-yyyy'
                });
                console.log(picker.get('disable'));
                console.log(picker.get('select'));
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_indikator(id_trx_cek_alat) {
        let alat_uji = $("select[name='alat_uji']").val();
        let waktu = $("select[name='waktu']").val();

        $(".list-indikator").html("");

        if (waktu) {
            $.ajax({
                url: base_url + 'cek_alat/request/get_indikator',
                data: {
                    alat_uji: alat_uji,
                    waktu: waktu,
                    id_trx_cek_alat: id_trx_cek_alat
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let html = "";
                    $.each(response, function(index, value) {
                        html += "<tr>" +
                            "<td>" + value.nama_indikator + "</td>" +
                            "<td class='text-center'><input " + (value.hasil_pemeriksaan ? (value.hasil_pemeriksaan == '1' ? "checked" : "") : "") + " required type='radio' value='1' class='form-check-input-styled' name='" + value.id_detail_indikator_alat_uji + "' /></td>" +
                            "<td class='text-center'><input " + (value.hasil_pemeriksaan ? (value.hasil_pemeriksaan == '2' ? "checked" : "") : "") + " required type='radio' value='2' class='form-check-input-styled' name='" + value.id_detail_indikator_alat_uji + "' /></td>" +
                            "</tr>";
                    });
                    $(".list-indikator").html(html);
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        }
    }

    $("#formSubmit").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: base_url + 'cek_alat/action_cek_alat',
            data: $(this).serialize(),
            type: 'POST',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_master_alat_uji']").val("");
                $("input[name='id_master_waktu']").val("");
                $(".alert_form").html("");
                $("#modalCekAlatUji").modal("toggle");
                $(".title_modal").html("");
                $(".nama-alat-uji").html("");
                $(".nama-waktu").html("");
                $(".list-indikator").html("");
                get_log();
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    });
</script>