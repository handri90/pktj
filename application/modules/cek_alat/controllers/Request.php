<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('cek_alat_model');
        $this->load->model('master_alat_uji/detail_indikator_alat_uji_model', 'detail_indikator_alat_uji_model');
        $this->load->model('master_waktu/master_waktu_model', 'master_waktu_model');
        $this->load->model('setting_waktu_alat_uji/setting_waktu_alat_uji_model', 'setting_waktu_alat_uji_model');
    }

    public function get_log()
    {
        $alat_uji = decrypt_data($this->iget("alat_uji"));
        $waktu = decrypt_data($this->iget("waktu"));

        $data_cek_alat = $this->cek_alat_model->get(
            array(
                "fields" => "id_trx_pemeriksaan_alat_uji,tanggal_pemeriksaan,
                GROUP_CONCAT(nama_indikator ORDER BY nama_indikator SEPARATOR '|') AS nama_indikator,
                GROUP_CONCAT(hasil_pemeriksaan ORDER BY nama_indikator SEPARATOR '|') AS hasil_pemeriksaan",
                "join" => array(
                    "setting_waktu_alat_uji" => "id_setting_waktu_alat_uji=setting_waktu_alat_uji_id",
                    "detail_indikator_alat_uji" => "id_detail_indikator_alat_uji=detail_indikator_alat_uji_id"
                ),
                "where" => array(
                    "petugas_pemroses" => $this->session->userdata('id_user'),
                    "master_waktu_id" => $waktu,
                    "master_alat_uji_id" => $alat_uji,
                ),
                "order_by" => array(
                    "tanggal_pemeriksaan" => "DESC"
                ),
                "group_by" => "tanggal_pemeriksaan"
            )
        );

        $templist = array();
        foreach ($data_cek_alat as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_pemeriksaan_alat_uji);
            $templist[$key]['tanggal_pemeriksaan_custom'] = longdate_indo($row->tanggal_pemeriksaan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_waktu()
    {
        $alat_uji = decrypt_data($this->iget("alat_uji"));
        $data_waktu = $this->setting_waktu_alat_uji_model->get(
            array(
                "fields" => "master_waktu.*",
                "join" => array(
                    "master_waktu" => "id_master_waktu=master_waktu_id AND master_waktu.deleted_at IS NULL",
                    "detail_indikator_alat_uji" => "detail_indikator_alat_uji_id=id_detail_indikator_alat_uji AND detail_indikator_alat_uji.deleted_at IS NULL",
                ),
                "where" => array(
                    "petugas_pemroses" => $this->session->userdata('id_user'),
                    "master_alat_uji_id" => $alat_uji
                ),
                "group_by" => "id_master_waktu"
            )
        );

        $templist = array();
        foreach ($data_waktu as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_waktu);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_indikator()
    {
        $alat_uji = decrypt_data($this->iget("alat_uji"));
        $waktu = decrypt_data($this->iget("waktu"));
        $id_trx_cek_alat = decrypt_data($this->iget("id_trx_cek_alat"));
        $tanggal_pemeriksaan_edit = "";

        if ($id_trx_cek_alat) {
            $data_trx_by_id = $this->cek_alat_model->get_by($id_trx_cek_alat);
            $tanggal_pemeriksaan_edit = $data_trx_by_id->tanggal_pemeriksaan;
        }

        $data_indikator = $this->detail_indikator_alat_uji_model->get(
            array(
                "fields" => "nama_indikator,id_detail_indikator_alat_uji,hasil_pemeriksaan",
                "join" => array(
                    "setting_waktu_alat_uji" => "id_detail_indikator_alat_uji=detail_indikator_alat_uji_id AND setting_waktu_alat_uji.deleted_at IS NULL",
                    "master_waktu" => "id_master_waktu = master_waktu_id"
                ),
                "left_join" => array(
                    "trx_pemeriksaan_alat_uji" => "setting_waktu_alat_uji_id=id_setting_waktu_alat_uji AND tanggal_pemeriksaan = '{$tanggal_pemeriksaan_edit}' AND trx_pemeriksaan_alat_uji.deleted_at IS NULL"
                ),
                "where" => array(
                    "petugas_pemroses" => $this->session->userdata('id_user'),
                    "master_waktu_id" => $waktu,
                    "master_alat_uji_id" => $alat_uji,
                ),
                "order_by" => array(
                    "nama_indikator" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_indikator as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_detail_indikator_alat_uji);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_set_waktu()
    {
        $alat_uji = decrypt_data($this->iget("alat_uji"));
        $waktu = decrypt_data($this->iget("waktu"));
        $id_trx_cek_alat = decrypt_data($this->iget("id_trx_cek_alat"));
        $tanggal_pemeriksaan_edit = array();

        $data_trx_by_id = "";

        if ($id_trx_cek_alat) {
            $data_trx_by_id = $this->cek_alat_model->get_by($id_trx_cek_alat);
            $expl_tanggal_pemeriksaan = explode("-", $data_trx_by_id->tanggal_pemeriksaan);
            array_push($tanggal_pemeriksaan_edit, (int) $expl_tanggal_pemeriksaan[0]);
            array_push($tanggal_pemeriksaan_edit, (int) $expl_tanggal_pemeriksaan[1] - 1);
            array_push($tanggal_pemeriksaan_edit, (int) $expl_tanggal_pemeriksaan[2]);
        }

        $data_indikator = $this->detail_indikator_alat_uji_model->query(
            "
            SELECT IFNULL(tanggal_pemeriksaan_terakhir,CURDATE()) AS tanggal_pemeriksaan_last_realtime,jumlah_hari
            FROM master_alat_uji
            INNER JOIN detail_indikator_alat_uji ON id_master_alat_uji=master_alat_uji_id AND detail_indikator_alat_uji.deleted_at IS NULL
            INNER JOIN setting_waktu_alat_uji ON id_detail_indikator_alat_uji=detail_indikator_alat_uji_id AND setting_waktu_alat_uji.deleted_at IS NULL
            INNER JOIN master_waktu ON id_master_waktu = master_waktu_id  AND master_waktu.deleted_at IS NULL
            LEFT JOIN 
            (
                SELECT tanggal_pemeriksaan AS tanggal_pemeriksaan_last_realtime,setting_waktu_alat_uji_id
                FROM trx_pemeriksaan_alat_uji
                WHERE id_trx_pemeriksaan_alat_uji IN 
                (
                SELECT MAX(id_trx_pemeriksaan_alat_uji) AS id_trx_pemeriksaan_alat_uji
                FROM trx_pemeriksaan_alat_uji
                WHERE trx_pemeriksaan_alat_uji.deleted_at IS NULL
                GROUP BY setting_waktu_alat_uji_id
                ) AND trx_pemeriksaan_alat_uji.deleted_at IS NULL
            ) AS b ON b.setting_waktu_alat_uji_id=id_setting_waktu_alat_uji
            WHERE petugas_pemroses = '{$this->session->userdata('id_user')}' AND id_master_alat_uji = '{$alat_uji}' AND id_master_waktu = '{$waktu}' AND master_alat_uji.deleted_at IS NULL
            GROUP BY id_master_waktu
            "
        )->row();


        $expl_tanggal_pemeriksaan_realtime = explode("-", $data_indikator->tanggal_pemeriksaan_last_realtime);
        $expl_today = explode("-", date("Y-m-d"));

        $rangeMulai = date_create(date("d-m-Y", mktime(0, 0, 0, date($expl_tanggal_pemeriksaan_realtime[1]), date($expl_tanggal_pemeriksaan_realtime[2]), date($expl_tanggal_pemeriksaan_realtime[0]))));
        $rangeSelesai = date_create(date("d-m-Y", mktime(0, 0, 0, date($expl_today[1]), date($expl_today[2]), date($expl_today[0]))));
        $dateTemp = date_diff($rangeMulai, $rangeSelesai);

        $rangeDate = $dateTemp->format("%a") + 1;

        $arr_inverted = array();

        $jumlah_hari = (int) $data_indikator->jumlah_hari;
        for ($n = 0; $n < $rangeDate; $n += $jumlah_hari) {
            $cek_tanggal = date("Y-m-d", mktime(0, 0, 0, date($expl_tanggal_pemeriksaan_realtime[1]), date($expl_tanggal_pemeriksaan_realtime[2]) + $n, date($expl_tanggal_pemeriksaan_realtime[0])));

            if ($data_trx_by_id) {
                if ($cek_tanggal == $data_trx_by_id->tanggal_pemeriksaan) {
                    $expl_date_inverted = explode("-", $cek_tanggal);
                    array_push($arr_inverted, array((int) $expl_date_inverted[0], (int) $expl_date_inverted[1] - 1, (int) $expl_date_inverted[2], 'inverted'));
                    continue;
                }
            }

            $cek_db_tanggal = $this->detail_indikator_alat_uji_model->query(
                "
                SELECT COUNT(*) AS a
                FROM (
                    SELECT tanggal_pemeriksaan
                    FROM master_alat_uji
                    INNER JOIN detail_indikator_alat_uji ON id_master_alat_uji=master_alat_uji_id AND detail_indikator_alat_uji.deleted_at IS NULL
                    INNER JOIN setting_waktu_alat_uji ON id_detail_indikator_alat_uji=detail_indikator_alat_uji_id AND setting_waktu_alat_uji.deleted_at IS NULL
                    INNER JOIN master_waktu ON id_master_waktu = master_waktu_id AND master_waktu.deleted_at IS NULL
                    INNER JOIN trx_pemeriksaan_alat_uji ON setting_waktu_alat_uji_id=id_setting_waktu_alat_uji AND trx_pemeriksaan_alat_uji.deleted_at IS NULL
                    WHERE petugas_pemroses = '{$this->session->userdata('id_user')}' AND id_master_alat_uji = '{$alat_uji}' AND id_master_waktu = '{$waktu}' AND master_alat_uji.deleted_at IS NULL
                ) AS tbl
                WHERE tanggal_pemeriksaan = '{$cek_tanggal}'
                GROUP BY tanggal_pemeriksaan
                "
            )->row();

            if (!$cek_db_tanggal) {
                $expl_date_inverted = explode("-", $cek_tanggal);
                array_push($arr_inverted, array((int) $expl_date_inverted[0], (int) $expl_date_inverted[1] - 1, (int) $expl_date_inverted[2], 'inverted'));
            }
        }

        $data = array('min' => array((int) $expl_tanggal_pemeriksaan_realtime[0], (int) $expl_tanggal_pemeriksaan_realtime[1] - 1, (int) $expl_tanggal_pemeriksaan_realtime[2]), 'max' => array((int) $expl_today[0], (int) $expl_today[1] - 1, (int) $expl_today[2]), "arr_inverted" => $arr_inverted, "tanggal_edit_pemeriksaan" => $tanggal_pemeriksaan_edit);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
