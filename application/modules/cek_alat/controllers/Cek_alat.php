<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cek_alat extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_alat_uji/master_alat_uji_model', 'master_alat_uji_model');
        $this->load->model('master_alat_uji/detail_indikator_alat_uji_model', 'detail_indikator_alat_uji_model');
        $this->load->model('setting_waktu_alat_uji/setting_waktu_alat_uji_model', 'setting_waktu_alat_uji_model');
        $this->load->model('cek_alat_model');
    }

    public function index()
    {
        $data['alat_uji'] = $this->master_alat_uji_model->get(
            array(
                "fields" => "master_alat_uji.*",
                "join" => array(
                    "detail_indikator_alat_uji" => "master_alat_uji_id=id_master_alat_uji",
                    "setting_waktu_alat_uji" => "detail_indikator_alat_uji_id=id_detail_indikator_alat_uji"
                ),
                "where" => array(
                    "petugas_pemroses" => $this->session->userdata("id_user")
                ),
                "order_by" => array(
                    "nama_alat_uji" => "ASC"
                ),
                "group_by" => "id_master_alat_uji"
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Cek Alat Uji', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function action_form_setting_waktu()
    {
        $id_setting_waktu_alat_uji = decrypt_data($this->ipost("id_setting_waktu_alat_uji"));
        $id_detail_indikator = decrypt_data($this->ipost("id_detail_indikator"));
        $waktu = decrypt_data($this->ipost("waktu"));
        $petugas = decrypt_data($this->ipost("petugas"));
        $tanggal_pemeriksaan_terakhir = $this->ipost("tanggal_pemeriksaan_terakhir");

        $date_exp = explode("/", $tanggal_pemeriksaan_terakhir);
        $tanggal_pemeriksaan = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

        if ($id_setting_waktu_alat_uji) {
            $data_setting_waktu = array(
                "master_waktu_id" => $waktu,
                "petugas_pemroses" => $petugas,
                "tanggal_pemeriksaan_terakhir" => $tanggal_pemeriksaan,
                'updated_at' => $this->datetime()
            );

            $status = $this->cek_alat_model->edit($id_setting_waktu_alat_uji, $data_setting_waktu);
        } else {
            $data_setting_waktu = array(
                "detail_indikator_alat_uji_id" => $id_detail_indikator,
                "master_waktu_id" => $waktu,
                "petugas_pemroses" => $petugas,
                "tanggal_pemeriksaan_terakhir" => $tanggal_pemeriksaan,
                'created_at' => $this->datetime()
            );

            $status = $this->cek_alat_model->save($data_setting_waktu);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_trx_alat_uji()
    {
        $alat_uji = decrypt_data($this->iget('alat_uji'));
        $waktu = decrypt_data($this->iget('waktu'));
        $id_trx_cek_alat = decrypt_data($this->iget('id_trx_cek_alat'));

        $data_trx_by_id = $this->cek_alat_model->get_by($id_trx_cek_alat);
        $tanggal_pemeriksaan_edit = $data_trx_by_id->tanggal_pemeriksaan;

        $data_indikator = $this->detail_indikator_alat_uji_model->get(
            array(
                "fields" => "id_trx_pemeriksaan_alat_uji",
                "join" => array(
                    "setting_waktu_alat_uji" => "id_detail_indikator_alat_uji=detail_indikator_alat_uji_id",
                    "master_waktu" => "id_master_waktu = master_waktu_id"
                ),
                "left_join" => array(
                    "trx_pemeriksaan_alat_uji" => "setting_waktu_alat_uji_id=id_setting_waktu_alat_uji AND tanggal_pemeriksaan = '{$tanggal_pemeriksaan_edit}'"
                ),
                "where" => array(
                    "petugas_pemroses" => $this->session->userdata('id_user'),
                    "master_waktu_id" => $waktu,
                    "master_alat_uji_id" => $alat_uji,
                ),
                "order_by" => array(
                    "nama_indikator" => "ASC"
                )
            )
        );

        foreach ($data_indikator as $key => $val) {
            $status = $this->cek_alat_model->remove($val->id_trx_pemeriksaan_alat_uji);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function action_cek_alat()
    {
        $id_master_alat_uji = decrypt_data($this->ipost("id_master_alat_uji"));
        $id_master_waktu = decrypt_data($this->ipost("id_master_waktu"));
        $tanggal_pemeriksaan = date("Y-m-d", strtotime($this->ipost("tanggal_pemeriksaan")));

        $data_indikator = $this->detail_indikator_alat_uji_model->get(
            array(
                "fields" => "nama_indikator,id_detail_indikator_alat_uji",
                "join" => array(
                    "setting_waktu_alat_uji" => "id_detail_indikator_alat_uji=detail_indikator_alat_uji_id",
                    "master_waktu" => "id_master_waktu = master_waktu_id"
                ),
                "where" => array(
                    "petugas_pemroses" => $this->session->userdata('id_user'),
                    "master_waktu_id" => $id_master_waktu,
                    "master_alat_uji_id" => $id_master_alat_uji,
                ),
                "order" => array(
                    "nama_indikator" => "ASC"
                )
            )
        );

        foreach ($data_indikator as $key => $val) {
            $cek_trx = $this->setting_waktu_alat_uji_model->get(
                array(
                    "join" => array(
                        "detail_indikator_alat_uji" => "id_detail_indikator_alat_uji=detail_indikator_alat_uji_id AND detail_indikator_alat_uji.deleted_at IS NULL"
                    ),
                    "left_join" => array(
                        "trx_pemeriksaan_alat_uji" => "setting_waktu_alat_uji_id=id_setting_waktu_alat_uji AND trx_pemeriksaan_alat_uji.deleted_at IS NULL AND tanggal_pemeriksaan = '{$tanggal_pemeriksaan}'"
                    ),
                    "where" => array(
                        "master_waktu_id" => $id_master_waktu,
                        "petugas_pemroses" => $this->session->userdata('id_user'),
                        "master_alat_uji_id" => $id_master_alat_uji,
                        "id_detail_indikator_alat_uji" => $val->id_detail_indikator_alat_uji
                    )
                ),
                "row"
            );

            if ($cek_trx) {
                if ($cek_trx->id_trx_pemeriksaan_alat_uji) {
                    $data_trx = array(
                        "tanggal_pemeriksaan" => $tanggal_pemeriksaan,
                        "hasil_pemeriksaan" => $this->ipost($val->id_detail_indikator_alat_uji),
                        "updated_at" => $this->datetime()
                    );
                    $this->cek_alat_model->edit($cek_trx->id_trx_pemeriksaan_alat_uji, $data_trx);
                } else {
                    $data_trx = array(
                        "tanggal_pemeriksaan" => $tanggal_pemeriksaan,
                        "setting_waktu_alat_uji_id" => $cek_trx->id_setting_waktu_alat_uji,
                        "hasil_pemeriksaan" => $this->ipost($val->id_detail_indikator_alat_uji),
                        "created_at" => $this->datetime()
                    );

                    $this->cek_alat_model->save($data_trx);
                }
            }
        }
    }
}
