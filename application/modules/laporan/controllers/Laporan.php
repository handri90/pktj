<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_alat_uji/master_alat_uji_model', 'master_alat_uji_model');
    }

    public function index()
    {
        $data['alat_uji'] = $this->master_alat_uji_model->get(
            array(
                "order_by" => array(
                    "nama_alat_uji" => "ASC"
                )
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Laporan Pemeriksaan Alat Uji', 'is_active' => true]];
        $this->execute('index', $data);
    }
}
