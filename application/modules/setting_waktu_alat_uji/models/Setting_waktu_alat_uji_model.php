<?php

class Setting_waktu_alat_uji_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "setting_waktu_alat_uji";
        $this->primary_id = "id_setting_waktu_alat_uji";
    }
}
