<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting_waktu_alat_uji extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_alat_uji/master_alat_uji_model', 'master_alat_uji_model');
        $this->load->model('setting_waktu_alat_uji_model');
    }

    public function index()
    {
        $data['alat_uji'] = $this->master_alat_uji_model->get(
            array(
                "order_by" => array(
                    "nama_alat_uji" => "ASC"
                )
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Setting Waktu Alat Uji', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function action_form_setting_waktu()
    {
        $id_setting_waktu_alat_uji = decrypt_data($this->ipost("id_setting_waktu_alat_uji"));
        $id_detail_indikator = decrypt_data($this->ipost("id_detail_indikator"));
        $waktu = decrypt_data($this->ipost("waktu"));
        $petugas = decrypt_data($this->ipost("petugas"));
        $tanggal_pemeriksaan_terakhir = $this->ipost("tanggal_pemeriksaan_terakhir");

        $date_exp = explode("/", $tanggal_pemeriksaan_terakhir);
        $tanggal_pemeriksaan = $date_exp[2] . "-" . $date_exp[1] . "-" . $date_exp[0];

        if ($id_setting_waktu_alat_uji) {
            $data_setting_waktu = array(
                "master_waktu_id" => $waktu,
                "petugas_pemroses" => $petugas,
                "tanggal_pemeriksaan_terakhir" => $tanggal_pemeriksaan,
                'updated_at' => $this->datetime()
            );

            $status = $this->setting_waktu_alat_uji_model->edit($id_setting_waktu_alat_uji, $data_setting_waktu);
        } else {
            $data_setting_waktu = array(
                "detail_indikator_alat_uji_id" => $id_detail_indikator,
                "master_waktu_id" => $waktu,
                "petugas_pemroses" => $petugas,
                "tanggal_pemeriksaan_terakhir" => $tanggal_pemeriksaan,
                'created_at' => $this->datetime()
            );

            $status = $this->setting_waktu_alat_uji_model->save($data_setting_waktu);
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function delete_setting_waktu_alat_uji()
    {
        $id_setting_waktu_alat_uji = decrypt_data($this->iget('id_setting_waktu_alat_uji'));
        $data_master = $this->setting_waktu_alat_uji_model->get_by($id_setting_waktu_alat_uji);

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->setting_waktu_alat_uji_model->remove($id_setting_waktu_alat_uji);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
