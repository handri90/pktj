<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('setting_waktu_alat_uji_model');
        $this->load->model('login_model');
        $this->load->model('master_alat_uji/detail_indikator_alat_uji_model', 'detail_indikator_alat_uji_model');
        $this->load->model('master_waktu/master_waktu_model', 'master_waktu_model');
    }

    public function get_data_setting_waktu_alat_uji()
    {
        $id_indikator_alat_uji = decrypt_data($this->iget("id_indikator_alat_uji"));
        $data_setting_waktu_alat_uji = $this->setting_waktu_alat_uji_model->get(
            array(
                "fields" => "setting_waktu_alat_uji.*,nama_waktu,nama_lengkap",
                "join" => array(
                    "master_waktu" => "id_master_waktu=master_waktu_id",
                    "user" => "id_user=petugas_pemroses"
                ),
                "where" => array(
                    "detail_indikator_alat_uji_id" => $id_indikator_alat_uji
                ),
                "order" => array(
                    "nama_waktu" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_setting_waktu_alat_uji as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_setting_waktu_alat_uji);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_indikator_alat_uji()
    {
        $alat_uji = decrypt_data($this->iget("alat_uji"));
        $data_indikator_alat_uji = $this->detail_indikator_alat_uji_model->get(
            array(
                "where" => array(
                    "master_alat_uji_id" => $alat_uji
                ),
                "order" => array(
                    "nama_indikator" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_indikator_alat_uji as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_detail_indikator_alat_uji);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_master_waktu()
    {
        $data_waktu = $this->master_waktu_model->get(
            array(
                "order_by" => array(
                    "nama_waktu" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_waktu as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_waktu);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_petugas_pemroses()
    {
        $data_petugas = $this->login_model->get(
            array(
                "where" => array(
                    "level_user_id" => "3"
                ),
                "order_by" => array(
                    "nama_lengkap" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_petugas as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_user);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_setting_waktu()
    {
        $id_setting_waktu_alat_uji = decrypt_data($this->iget("id_setting_waktu_alat_uji"));
        $data = $this->setting_waktu_alat_uji_model->get(
            array(
                "fields" => "setting_waktu_alat_uji.*,DATE_FORMAT(tanggal_pemeriksaan_terakhir,'%d/%m/%Y') AS tanggal_pemeriksaan",
                "where" => array(
                    "id_setting_waktu_alat_uji" => $id_setting_waktu_alat_uji
                )
            ),
            "row"
        );

        $data->id_encrypt = encrypt_data($data->id_setting_waktu_alat_uji);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function check_waktu_double()
    {
        $id_setting_waktu_alat_uji = decrypt_data($this->iget("id_setting_waktu_alat_uji"));
        $id_detail_indikator = decrypt_data($this->iget("id_detail_indikator"));
        $waktu = decrypt_data($this->iget("waktu"));

        $wh = "";
        if ($id_setting_waktu_alat_uji) {
            $wh = "id_setting_waktu_alat_uji != '{$id_setting_waktu_alat_uji}'";
        }
        $data_setting_waktu = $this->setting_waktu_alat_uji_model->get(
            array(
                "fields" => "count(*) AS jumlah_pemakaian_waktu",
                "where" => array(
                    "detail_indikator_alat_uji_id" => $id_detail_indikator,
                    "master_waktu_id" => $waktu
                ),
                "where_false" => $wh,
                "group_by" => "detail_indikator_alat_uji_id"
            ),
            "row"
        );

        if ($data_setting_waktu) {
            if ($data_setting_waktu->jumlah_pemakaian_waktu > 0) {
                $data = true;
            } else {
                $data = false;
            }
        } else {
            $data = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
