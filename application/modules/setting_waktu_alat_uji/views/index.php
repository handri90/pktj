<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Alat Uji : </label>
                        <select class="form-control select-search" name="alat_uji" onChange="get_indikator_alat_uji()">
                            <option value="">-- Pilih Alat Uji --</option>
                            <?php
                            foreach ($alat_uji as $key => $row) {
                            ?>
                                <option value="<?php echo encrypt_data($row->id_master_alat_uji); ?>"><?php echo $row->nama_alat_uji; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card border-top-success card-hidden">
        <input type="hidden" name="id_indikator_alat_uji" />
        <div class="card-body">
            <div class="row list-alat-uji">

            </div>
        </div>
    </div>
    <!-- Basic datatable -->
    <div class="card card-hidden">
        <div class="card-body">
            <div class="text-right">
                <a href="#" onclick="show_form()" class="btn btn-info">Tambah Waktu Pemeriksaan</a>
            </div>
        </div>
        <table id="datatableAlatUji" class="table datatable-save-state">
            <thead>
                <tr>
                    <th>Nama Alat Uji</th>
                    <th>Petugas Pemroses</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->
</div>

<div id="modalWaktuAlatUji" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Waktu</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-5">Waktu</label>
                    <div class="col-lg-7">
                        <input type="hidden" class="form-control" name="id_setting_waktu_alat_uji">
                        <select class="form-control select-search" name="waktu">
                            <option value="">-- Pilih Waktu --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-5">Petugas</label>
                    <div class="col-lg-7">
                        <select class="form-control select-search" name="petugas">
                            <option value="">-- Pilih Petugas --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-5">Tanggal Pemeriksaan Terakhir</label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control daterange-single" readonly name="tanggal_pemeriksaan_terakhir" required placeholder="Tanggal Pemeriksaan Terakhir">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_setting_waktu()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".card-hidden").hide();

    let datatableAlatUji = $("#datatableAlatUji").DataTable({
        "columns": [
            null,
            null,
            {
                "width": "20%"
            }
        ]
    });

    function get_indikator_alat_uji() {
        let alat_uji = $("select[name='alat_uji']").val();
        $("input[name='id_indikator_alat_uji']").val("");

        if (alat_uji) {
            $.ajax({
                url: base_url + 'setting_waktu_alat_uji/request/get_indikator_alat_uji',
                data: {
                    alat_uji: alat_uji
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $(".card-hidden").show();
                    let html = "";
                    $.each(response, function(index, value) {
                        if (index == 0) {
                            $("input[name='id_indikator_alat_uji']").val(value.id_encrypt);
                        }
                        html += "<h4><a href='#' data-id='" + value.id_encrypt + "' onclick='show_setting_waktu(this)'> <span class='badge " + (index == 0 ? 'badge-info' : 'badge-light') + " mr-1'>" + value.nama_indikator + "</span></a></h4>";
                    });
                    $(".list-alat-uji").html(html);
                    get_data_waktu_indikator();
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        } else {
            $(".card-hidden").hide();
        }
    }

    function show_setting_waktu(e) {
        $(".list-alat-uji").find(".badge-info").removeClass("badge-info").addClass("badge-light");
        $(e).find(".badge-light").removeClass("badge-light").addClass("badge-info");
        $("input[name='id_indikator_alat_uji']").val($(e).attr("data-id"));
        get_data_waktu_indikator();
    }

    function get_data_waktu_indikator() {
        let id_detail_indikator = $("input[name='id_indikator_alat_uji']").val();
        datatableAlatUji.clear().draw();
        $.ajax({
            url: base_url + 'setting_waktu_alat_uji/request/get_data_setting_waktu_alat_uji',
            data: {
                id_indikator_alat_uji: id_detail_indikator
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    datatableAlatUji.row.add([
                        value.nama_waktu,
                        value.nama_lengkap,
                        "<a href='#' onclick=\"show_form_edit('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_setting_waktu_alat_uji) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'setting_waktu_alat_uji/delete_setting_waktu_alat_uji',
                    data: {
                        id_setting_waktu_alat_uji: id_setting_waktu_alat_uji
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_waktu_indikator();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_waktu_indikator();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_waktu_indikator();
                    }
                });
            }
        });
    }

    function show_form_edit(id_setting_waktu_alat_uji) {
        $.ajax({
            url: base_url + 'setting_waktu_alat_uji/request/get_detail_setting_waktu',
            data: {
                id_setting_waktu_alat_uji: id_setting_waktu_alat_uji
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_setting_waktu_alat_uji']").val(response.id_encrypt);
                $("input[name='tanggal_pemeriksaan_terakhir']").val(response.tanggal_pemeriksaan);
                $(".alert_form").html("");
                $("#modalWaktuAlatUji").modal("show");
                $(".title_modal").html("Ubah");
                get_master_waktu(response.master_waktu_id);
                get_petugas(response.petugas_pemroses);
                call_date_range();
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function show_form() {
        $("input[name='id_setting_waktu_alat_uji']").val("");
        $("input[name='tanggal_pemeriksaan_terakhir']").val(moment().format('DD/MM/YYYY'));
        $(".alert_form").html("");
        $("#modalWaktuAlatUji").modal("show");
        $(".title_modal").html("Tambah");
        get_master_waktu();
        get_petugas();
        call_date_range();
    }

    function get_master_waktu(parent_id_waktu = '') {
        let html = "";
        html += "<option value=''>-- Pilih Waktu --</option>";
        $.ajax({
            url: base_url + 'setting_waktu_alat_uji/request/get_master_waktu',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (parent_id_waktu == value.id_master_waktu) {
                        selected = "selected";
                    }
                    html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.nama_waktu + "</option>";
                });
                $("select[name='waktu']").html(html);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function get_petugas(parent_id_petugas = '') {
        let html = "";
        html += "<option value=''>-- Pilih Petugas --</option>";
        $.ajax({
            url: base_url + 'setting_waktu_alat_uji/request/get_petugas_pemroses',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (parent_id_petugas == value.id_user) {
                        selected = "selected";
                    }
                    html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.nama_lengkap + "</option>";
                });
                $("select[name='petugas']").html(html);
            },
            complete: function(response) {
                HoldOn.close();
            }
        });
    }

    function action_form_setting_waktu() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_setting_waktu_alat_uji = $("input[name='id_setting_waktu_alat_uji']").val();
        let id_detail_indikator = $("input[name='id_indikator_alat_uji']").val();
        let waktu = $("select[name='waktu']").val();
        let petugas = $("select[name='petugas']").val();
        let tanggal_pemeriksaan_terakhir = $("input[name='tanggal_pemeriksaan_terakhir']").val();

        $(".alert_form").html("");

        if (!waktu) {
            $(".alert_form").html("<div class='alert alert-danger'>Waktu tidak boleh kosong.</div>");
        } else if (!petugas) {
            $(".alert_form").html("<div class='alert alert-danger'>Petugas tidak boleh kosong.</div>");
        } else if (!tanggal_pemeriksaan_terakhir) {
            $(".alert_form").html("<div class='alert alert-danger'>Tanggal Pemeriksaan Terakhir tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'setting_waktu_alat_uji/request/check_waktu_double',
                data: {
                    id_setting_waktu_alat_uji: id_setting_waktu_alat_uji,
                    id_detail_indikator: id_detail_indikator,
                    waktu: waktu
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    if (response) {
                        $(".alert_form").html("<div class='alert alert-danger'>Waktu sudah digunakan untuk indikator yang sama.</div>");
                    } else {
                        $.ajax({
                            url: base_url + 'setting_waktu_alat_uji/action_form_setting_waktu',
                            data: {
                                id_setting_waktu_alat_uji: id_setting_waktu_alat_uji,
                                id_detail_indikator: id_detail_indikator,
                                waktu: waktu,
                                petugas: petugas,
                                tanggal_pemeriksaan_terakhir: tanggal_pemeriksaan_terakhir
                            },
                            type: 'POST',
                            beforeSend: function() {
                                HoldOn.open(optionsHoldOn);
                            },
                            success: function(response) {
                                $("input[name='id_setting_waktu_alat_uji']").val("");
                                $("input[name='tanggal_pemeriksaan_terakhir']").val("");
                                if (response) {
                                    $("#modalWaktuAlatUji").modal("toggle");
                                    get_data_waktu_indikator();
                                    swalInit(
                                        'Berhasil',
                                        'Data berhasil diubah',
                                        'success'
                                    );
                                } else {
                                    $("#modalWaktuAlatUji").modal("toggle");
                                    get_data_waktu_indikator();
                                    swalInit(
                                        'Gagal',
                                        'Data tidak bisa diubah',
                                        'error'
                                    );
                                }
                            },
                            complete: function() {
                                HoldOn.close();
                            }
                        });
                    }
                },
                complete: function(response) {
                    HoldOn.close();
                }
            });
        }
    }

    function call_date_range() {
        $('.daterange-single').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            },
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'), 10)
        }, function(chosen_date) {
            $('input[name="tanggal_pemeriksaan_terakhir"]').val(chosen_date.format('DD/MM/YYYY'));
        });
    }
</script>