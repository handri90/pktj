<div class="content">
    <span class="form_error"></span>
    <div class="card">
        <?php echo form_open(current_url(), array("class" => "wizard-form steps-basic")); ?>
        <h6>Nama Alat Uji</h6>
        <fieldset>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Nama alat Uji <span class="text-danger">*</span></label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_alat_uji : ""; ?>" name="nama_alat_uji" required placeholder="Nama Alat Uji">
                </div>
            </div>
        </fieldset>

        <h6>Your education</h6>
        <fieldset>
            <div class="card-body">
                <div class="text-right">
                    <span class="list_delete_indikator"></span>
                    <a href="#tambah" class="btn btn-primary tambah_field_indikator"><i class="icon-plus2"></i></a>
                </div>
            </div>
            <div class="form-group row">
                <div class="card card-table table-responsive shadow-0 mb-0">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Indikator Yang Diperiksa</th>
                                <th>SOP Pemeriksaan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($content)) {
                            ?>
                                <tr class="const_form d-none">
                                    <td><input type="text" class="form-control n-indikator" /></td>
                                    <td><textarea class="form-control n-sop-pemeriksaan"></textarea></td>
                                    <td><span class="close_single"></span></li>
                                    </td>
                                </tr>
                                <?php
                                $id_detail_indikator = explode("|", $content->id_detail_indikator_alat_uji);
                                $nama_indikator = explode("|", $content->nama_indikator);
                                $sop_pemeriksaan = explode("|", $content->sop_pemeriksaan);

                                foreach ($id_detail_indikator as $key => $val) {
                                ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="id_detail_indikator[]" value="<?php echo $id_detail_indikator[$key]; ?>" />
                                            <input type="text" class="form-control indikator" name="old_indikator[<?php echo $id_detail_indikator[$key]; ?>]" value="<?php echo $nama_indikator[$key]; ?>" />
                                        </td>
                                        <td><textarea class="form-control sop_pemeriksaan" name="old_sop_pemeriksaan[<?php echo $id_detail_indikator[$key]; ?>]"><?php echo $sop_pemeriksaan[$key]; ?></textarea></td>
                                        <td><span class="close_single"><a href="#" data-id-detail-indikator="<?php echo $id_detail_indikator[$key]; ?>" onclick="old_remove_panel(this)" class="btn btn-sm btn-danger"><i class="icon-close2"></i></a></span></li>
                                        </td>
                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                <tr class="const_form">
                                    <td><input type="text" class="form-control indikator" name="indikator[]" /></td>
                                    <td><textarea class="form-control sop_pemeriksaan" name="sop_pemeriksaan[]"></textarea></td>
                                    <td><span class="close_single"></span></li>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
        </form>
        <?php echo form_close(); ?>
    </div>
</div>

<script>
    // Basic wizard setup
    $('.steps-basic').steps({
        headerTag: 'h6',
        bodyTag: 'fieldset',
        transitionEffect: 'fade',
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
            next: 'Next <i class="icon-arrow-right14 ml-2" />',
            finish: 'Simpan <i class="icon-arrow-right14 ml-2" />'
        },
        onFinished: function(event, currentIndex) {
            let name_uji_alat = $("input[name='nama_alat_uji']");
            let bool_indikator = true;
            let bool_sop = true;

            if ($(".indikator").length > 0) {
                $.each($("input[name='indikator[]']"), function(key, value) {
                    if ($(this).val() == "") {
                        bool_indikator = false;
                    }
                });
            } else {
                bool_indikator = false;
            }

            if ($(".sop_pemeriksaan").length > 0) {
                $.each($("textarea[name='sop_pemeriksaan[]']"), function(key, value) {
                    if ($(this).val() == "") {
                        bool_sop = false;
                    }
                });
            } else {
                bool_sop = false;
            }

            if (!name_uji_alat.val()) {
                $(".form_error").html('<div class="alert alert-danger border-0"><span class="font-weight-semibold">Nama Uji Alat Tidak Boleh Kosong</div>');
            } else if (!bool_indikator) {
                $(".form_error").html('<div class="alert alert-danger border-0"><span class="font-weight-semibold">Indikator Tidak Boleh Kosong</div>');
            } else if (!bool_sop) {
                $(".form_error").html('<div class="alert alert-danger border-0"><span class="font-weight-semibold">SOP Pemeriksaan Tidak Boleh Kosong</div>');
            } else {
                $(".form_error").html("");
                let form = $(this);

                form.submit();
            }

        }
    });

    $(".tambah_field_indikator").on('click', function(e) {
        html_ref = "";
        let html_clone = $(".const_form").clone(true);
        html_clone.find('[type=text]').val('');
        html_clone.find('textarea').val('');
        html_clone.find('.n-indikator').attr('name', 'indikator[]');
        html_clone.find('.n-indikator').addClass('indikator');
        html_clone.find('.n-sop-pemeriksaan').attr('name', 'sop_pemeriksaan[]');
        html_clone.find('.n-sop-pemeriksaan').addClass('sop_pemeriksaan');
        html_clone.removeClass("const_form");
        html_clone.removeClass("n-indikator");
        html_clone.removeClass("n-sop-pemeriksaan");
        html_clone.removeClass("d-none");
        html_clone.find('.close_single').append('<a href="#" class="remove_panel btn btn-sm btn-danger"><i class="icon-close2"></i></a>');
        $(".table tr:last").after(html_clone);

        $(".remove_panel").on('click', function(e) {
            e.preventDefault();
            $(this).parent().parent().parent().remove();
        });
    });

    function old_remove_panel(e) {
        $(e).parent().parent().parent().remove();
        let id = $(e).attr("data-id-detail-indikator");
        $(".list_delete_indikator").append("<input type='hidden' name='delete_indiaktor[]' value='" + id + "' />");
    }
</script>